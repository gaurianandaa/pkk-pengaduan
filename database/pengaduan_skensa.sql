-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 15 Nov 2023 pada 23.56
-- Versi server: 8.0.30
-- Versi PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pengaduan_skensa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `reports`
--

CREATE TABLE `reports` (
  `id_report` int NOT NULL,
  `date` date NOT NULL,
  `isi_laporan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `image` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `nis` varchar(191) NOT NULL,
  `status` enum('Pending','Completed') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data untuk tabel `reports`
--

INSERT INTO `reports` (`id_report`, `date`, `isi_laporan`, `image`, `nis`, `status`) VALUES
(1, '2023-11-14', 'Beberapa kali saya melihat sampah berserakan di sekitar area sekolah, terutama di halaman dan lorong-lorong. Saya mengajukan permohonan agar petugas kebersihan dapat lebih aktif membersihkan area tersebut dan memastikan tempat-tempat sampah tersedia di lokasi yang strategis.', '1700005832-report1.jpeg', '29702', 'Pending'),
(2, '2023-11-14', 'Saya telah memperhatikan bahwa kondisi toilet di sekolah tidak selalu bersih. Ada beberapa kejadian di mana toilet tidak tersedia perlengkapan kebersihan seperti sabun dan tisu kertas. Saya berharap pihak sekolah dapat memastikan bahwa toilet selalu dalam kondisi bersih dan dilengkapi dengan semua perlengkapan yang diperlukan.', '1700005954-report2.jpeg', '29702', 'Pending'),
(3, '2023-11-15', 'osis', '1700009990-gam3.jpg', '29702', 'Pending'),
(4, '2023-11-15', 'Saya ingin menyarankan agar sekolah memiliki program pengelolaan limbah yang lebih efektif. Mungkin dapat dipertimbangkan untuk memasang tempat sampah terpisah untuk kertas, plastik, dan bahan daur ulang lainnya. Hal ini dapat membantu mengajarkan nilai-nilai lingkungan kepada para murid.', '1700010236-report1.jpeg', '29702', 'Pending');

-- --------------------------------------------------------

--
-- Struktur dari tabel `responses`
--

CREATE TABLE `responses` (
  `id_feedback` int NOT NULL,
  `id_report` int NOT NULL,
  `response` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `nis` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data untuk tabel `responses`
--

INSERT INTO `responses` (`id_feedback`, `id_report`, `response`, `date`, `nis`) VALUES
(1, 2, 'Laporan telah diterima dan akan ditindak lanjuti, terimakasih atas laporan anda', '2023-11-14', '29702'),
(2, 1, 'sudah di terima\r\n', '2023-11-15', '29702');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `nis` varchar(191) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `role` enum('Admin','Siswa') NOT NULL DEFAULT 'Siswa',
  `password` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`nis`, `name`, `phone`, `role`, `password`) VALUES
('199205142023052008', 'TUTIK HARYATI, S.Pd.', '08981928192', 'Admin', '$2y$10$esAm6gi0K.wkw1eR053YMubyvDMAgBia6ainGTcn.Z3Oyxd53Y.0O'),
('29699', 'wanda', '123', 'Admin', '$2y$10$CDOHH7PmM6SFnOgelN6I9.kIky0CbZfxjCuN9S8LOnFP6JUsyZRVS'),
('29701', 'ulan', '085858729918', 'Siswa', '$2y$10$GnD4qLQ5RXGsmEuhNgxuyuErcrCq0bKuNylvVNjqRa.Y67FvRFIsO'),
('29702', 'gauri', '085858729918', 'Siswa', '$2y$10$VXbUZ7tEjYGcPe7ALHpNXOaQpAApbbc8Z.xJiqu8O4svZkTy1KSb6');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id_report`),
  ADD KEY `nis` (`nis`);

--
-- Indeks untuk tabel `responses`
--
ALTER TABLE `responses`
  ADD PRIMARY KEY (`id_feedback`),
  ADD KEY `id_report` (`id_report`),
  ADD KEY `nis` (`nis`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`nis`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `reports`
--
ALTER TABLE `reports`
  MODIFY `id_report` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `responses`
--
ALTER TABLE `responses`
  MODIFY `id_feedback` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_ibfk_1` FOREIGN KEY (`nis`) REFERENCES `users` (`nis`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ketidakleluasaan untuk tabel `responses`
--
ALTER TABLE `responses`
  ADD CONSTRAINT `responses_ibfk_1` FOREIGN KEY (`id_report`) REFERENCES `reports` (`id_report`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `responses_ibfk_2` FOREIGN KEY (`nis`) REFERENCES `users` (`nis`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
