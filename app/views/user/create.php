<!-- create report -->

<!-- start content -->
<!-- <section class="d-flex justify-content-center mx-5 mt-2">
    <div class="content">
        <div class="row ">
            <div class="col-lg-8 mt-5 w-100">
                <div class="card-create border h-100 py-2 mx-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col-lg-12 mr-2">
                                <div class="text-judul text-xs font-weight-bold  text-uppercase">
                                    <h6>Create Complaint</h6>
                                </div>
                            </div>
                            <form action="<?= BASEURL ?>/user/prosesCreate" method="post">
                                <div class="col-lg-12 ">
                                    <label for="date" class="form-label"></label>
                                    <input class="form-control" type="date" name="date" id="date" placeholder="Tanggal">
                                </div>
                                <div class="col-lg-12">
                                    <label for="isi_laporan" class="form-label"></label>
                                    <input class="form-control py-5" type="text" name="isi_laporan" id="isi_laporan" placeholder="Buat Laporan....">
                                </div>
                                <div class="col-lg-12 ">
                                    <label for="image" class="form-label"></label>
                                    <input class="form-control py-5" type="file" name="image" id="image" placeholder="Bukti Foto....">
                                </div>
                                <input class="form-control py-5" type="text" name="id_user" id="" value="29701" hidden>
                                <input class="form-control py-5" type="text" name="status" id="" value="pending" hidden>
                                <button type="submit" class="btn-content btn-content border mt-5 py-2 px-3">Submit</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-1 mt-3">
            <img class="img-create" src="img/gam1.png" alt="">
        </div>
    </div>
    </div>
</section> -->

<div class="container my-5">
    <div class="row d-flex justify-content-center">
        <div class="col-sm-8 col-lg-6 mg-t-10 mg-lg-t-0">
            <div class="card shadow-lg border-0">
                <div class="card-header">
                    <h5 id="section2" class="mg-b-10">Create Report</h5>
                </div>
                <div class="card-body p-4 mb-4">
                    <form action="<?= BASEURL; ?>/user/createReport" method="post" enctype="multipart/form-data">
                        <div class="col-lg-12 ">
                            <label for="date" class="form-label"></label>
                            <input class="form-control" type="date" name="date" id="date" placeholder="Tanggal" value="<?= date('Y-m-d') ?>" hidden>
                        </div>
                        <div class="col-lg-12 mb-4">
                            <label for="isi_laporan" class="form-label"></label>
                            <input class="form-control py-5" type="text" name="isi_laporan" id="isi_laporan" placeholder="Buat Laporan....">
                        </div>
                        <div class="col-lg-12 ">
                            <label for="image" class="form-label"></label>
                            <input class="form-control py-5" type="file" name="image" id="image" placeholder="Bukti Foto....">
                        </div>
                        <input class="form-control py-5" type="text" name="nis" id="" value="29702" hidden>
                        <input class="form-control py-5" type="text" name="status" id="" value="Pending" hidden>
                        <div class="text-end">
                            <button type="submit" class="btn text-light mt-3">
                                <i class="fa-solid fa-floppy-disk"></i> Submit
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- 
<style>
    #drop-area {
        border: 2px dotted #ccc;
        text-align: center;
        cursor: pointer;
    }
</style>

<script>
    const dropArea = document.getElementById('drop-area');
    const imageInput = document.getElementById('image');
    const previewImage = document.getElementById('previewImage');

    imageInput.addEventListener('change', function() {
        const file = imageInput.files[0];
        if (file) {
            const reader = new FileReader();

            reader.onload = function(e) {
                previewImage.src = e.target.result;
            };

            reader.readAsDataURL(file);
        } else {
            previewImage.src = '';
        }
    })

    dropArea.addEventListener('dragover', (e) => {
        e.preventDefault();
        dropArea.style.border = '2px dashed #000';
    });

    dropArea.addEventListener('dragleave', () => {
        dropArea.style.border = '2px dashed #ccc';
    });

    dropArea.addEventListener('drop', (e) => {
        e.preventDefault();
        dropArea.style.border = '2px dashed #ccc';
        const file = e.dataTransfer.files[0];
        imageInput.files = e.dataTransfer.files;
    });

    dropArea.addEventListener('click', () => {
        imageInput.click();
    });
</script> -->