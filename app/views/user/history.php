<div class="section mx-5 mt-5" style="width: 100%;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>History</h1>
            </div>
        </div>
        <div class="row border mt-4">
            <div class="col-lg-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col-lg-3">No</th>
                            <th scope="col-lg-3">Tanggal</th>
                            <th scope="col-lg-3">Laporan</th>
                            <th scope="col-lg-4">Status</th>
                            <th scope="col-lg-3">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach ($data['history'] as $history) : ?>
                            <tr>
                                <th scope="row"><?= $history['id_report'] ?></th>
                                <td><?= $history['date'] ?></td>
                                <td><?= $history['isi_laporan'] ?></td>
                                <td>
                                    <?php
                                        $statusLabel = 'Terkirim'; // Default status
                                        foreach ($data['response'] as $response) {
                                            if ($response['id_report'] == $history['id_report']) {
                                                $statusLabel = 'Completed';
                                                break; // Keluar dari loop jika respons ditemukan
                                            }
                                        }
                                    ?>
                                    <label type="label" name="status" class="btn-content btn-content border py-1 px-3"><?= $statusLabel ?></label>
                                </td>
                                <td>
                                    <button class="btn btn-primary p-2" style="width: fit-content;" data-toggle="modal" data-target="#detail<?= $history['id_report'] ?>">
                                        View
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>

<?php foreach ($data['history'] as $history) : ?>
    <div class="modal fade" id="detail<?= $history['id_report'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content container w-100">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Report</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> ×</span>
                    </button>
                </div>

                <!-- modal-content -->
                <div class="modal-body m-4">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table border-0 table-hover p-2">
                                <tbody>
                                    <tr>
                                        <td style="width: 30%;">NIS</td>
                                        <td><?= $history['nis'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30%;">FROM</td>
                                        <td>ulan</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30%;">REPORT DATE</td>
                                        <td><?= $history['date'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30%;">REPORT DETAIL</td>
                                        <td><?= $history['isi_laporan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30%;">PROOF IMAGE</td>
                                        <td><img src="<?= HREF ?>/img/<?= $history['image'] ?>" alt="" style="width: 300px; height:300px;"></td>
                                    </tr>
                                    <?php foreach ($data['response'] as $response) : ?>
                                        <?php if ($response['id_report'] == $history['id_report']) : ?>
                                            <tr>
                                                <td style="width: 30%;">FEEDBACK</td>
                                                <td><?= $response['response'] ?></td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach ?>