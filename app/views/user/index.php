<section class="d-flex justify-content-center w-100">
    <div class="content">
        <div class="row ">
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-lg-12 mt-5">
                <div class="card-atas border h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold  text-uppercase mb-1">
                                    Welcome Back</div>
                                <p>Welcome to the SMK 1 Denpasar <br> school website,
                                    please give your complaints about this school to
                                    create a good school atmosphere</p>
                                <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                                <button type="text" class="btn-content btn-content border-0 mt-5 py-2 px-3">create</button>
                            </div>
                            <div class="col-auto">
                                <i class=""></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mt-5">
            <div class="col-lg-6 mt-5">
                <div class="card-containt border h-100 py-5">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <a class="text-xs font-weight-bold  text-uppercase mb-1 text-decoration-none text-light" href="<?= BASEURL ?>/user/create">
                                    Create Report</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mt-5">
                <div class="card-containt border h-100 py-5">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <a class="text-xs font-weight-bold  text-uppercase mb-1 text-decoration-none text-light" href="<?= BASEURL ?>/user/history">
                                    Your History</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>