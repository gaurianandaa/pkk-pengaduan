
<!-- Bootstrap core JavaScript-->
<script src="<?= HREF ?>/vendor/jquery/jquery.min.js"></script>
<script src="<?= HREF ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

<!-- Core plugin JavaScript-->
<script src="<?= HREF ?>/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?= HREF ?>/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="<?= HREF ?>/vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="<?= HREF ?>/js/demo/chart-area-demo.js"></script>
<script src="<?= HREF ?>/js/demo/chart-pie-demo.js"></script>