<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SKENPORT || <?= $data['title'] ?></title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  <link rel="stylesheet" href="<?= HREF ?>/css/bootstrap.css">
  <link rel="stylesheet" href="<?= HREF ?>/cssNew/user.css">
  <link href="<?= HREF ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
</head>

<body>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

  </head>

  <body>
    <!-- Page Wrapper -->
    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- sidebar - brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-start" href="<?= BASEURL ?>/admin">
          <img src="<?= HREF ?>/img/admin/logoSkensa.png" alt="" style="width: 40px; height:40px;">
          <div class="sidebar-brand-text mx-1 fw-light">SKENPORT</div>
        </a>


        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
          <img class="img-sideuser rounded-circle align-items-center mt-1 " src="<?= HREF ?>/img/admin/profileDummy.png" alt="" width="100" height="70">
        </a>
        <h6 class="text-light">Ulan</h6>


        <!-- Divider -->
        <hr class="bg-white">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active mx-4">
          <a class="nav-link" href="<?= BASEURL ?>/user">
            <i class="fa-solid fa-house"></i>
            <span>Dashboard</span></a>
        </li>
        <li class="nav-item active mx-4">
          <a class="nav-link" href="<?= BASEURL ?>/user/create">
            <i class=""></i>
            <span>Create</span></a>
        </li>
        <li class="nav-item active mx-4">
          <!-- /user tu nama controllernya, /history tu nama method -->
          <!-- jadi ini cuma buat ngebuka file doang -->
          <a class="nav-link" href="<?= BASEURL ?>/user/history">
            <i class=""></i>
            <span>History</span></a>
        </li>
        <li class="nav-item active mx-4">
          <!-- /user tu nama controllernya, /history tu nama method -->
          <!-- jadi ini cuma buat ngebuka file doang -->
          <a class="nav-link" href="<?= BASEURL ?>/login/logout">
            <i class=""></i>
            <span>Logout</span></a>
        </li>
      </ul>
      <!-- End of Sidebar -->
  </body>