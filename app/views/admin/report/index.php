<!-- report -->

<div class="container m-3">
    <h1>Management <?= $data['title'] ?></h1>

    <div class="row my-5">
        <div class="col-lg-12 d-flex justify-content-end">
            <form class="d-flex" action="<?= BASEURL ?>/report/index" method="post">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" style="width: fit-content;" name="search">
                <button class="btn btn-outline-primary" type="submit">Search</button>
            </form>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th style="width: 10%;">No</th>
                <th style="width: 20%;">Tanggal</th>
                <th style="width: 20%;">Laporan</th>
                <th style="width: 20%;">Nis</th>
                <th style="width: 10%;">Status</th>
                <th style="width: 20%;" class="text-center">Action</th>
            </tr>
        </thead>
        <?php foreach ($data['report'] as $report) : ?>
            <tbody>
                <tr>
                    <td><?= $report['id_report'] ?></td>
                    <td><?= $report['date'] ?></td>
                    <td><?= substr($report['isi_laporan'], 0, 50); ?>...</td>
                    <td><?= $report['nis'] ?></td>
                    <td>
                        <?php
                        $statusLabel = 'Pending'; // Default status
                        foreach ($data['response'] as $response) {
                            if ($response['id_report'] == $report['id_report']) {
                                $statusLabel = 'Completed';
                                break; // Keluar dari loop jika respons ditemukan
                            }
                        }
                        // var_dump($statusLabel);
                        // die;
                        ?>
                        <?= $statusLabel ?>
                        
                    </td>
                    <td>
                        <div class="text-center">
                            <button class="btn btn-primary p-2" style="width: fit-content;" data-toggle="modal" data-target="#detail<?= $report['id_report'] ?>">
                                View
                            </button>
                        </div>
                    </td>
                </tr>
            </tbody>
        <?php endforeach ?>
    </table>
</div>


<?php foreach ($data['report'] as $report) : ?>
    <div class="modal fade" id="detail<?= $report['id_report'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content container w-100">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Report</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> ×</span>
                    </button>
                </div>
                <div class="modal-body m-4">
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-8">
                            <table class="table border-0 table-hover p-2">
                                <tbody>
                                    <tr>
                                        <td style="width: 40%;">NIS</td>
                                        <!-- <td><?= $report['nis'] ?></td> -->
                                        <td><?= str_repeat('$x*', strlen($report['nis'])) ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 40%;">FROM</td>
                                        <td><?= str_repeat('$.*', strlen($report['nis'])) ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 40%;">REPORT DATE</td>
                                        <td><?= $report['date'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 40%;">REPORT DETAIL</td>
                                        <td><?= $report['isi_laporan'] ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 40%;">PROOF IMAGE</td>
                                        <td><img src="<?= HREF ?>/img/<?= $report['image'] ?>" alt="" style="width: 300px; height:300px;"></td>
                                    </tr>
                                    <?php foreach ($data['response'] as $response) : ?>
                                        <?php if ($response['id_report'] == $report['id_report']) : ?>
                                            <tr>
                                                <td style="width: 30%;">FEEDBACK</td>
                                                <td><?= $response['response'] ?></td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <?php
                        $statusLabel = 'Pending'; // Default 
                        foreach ($data['response'] as $response) {
                            if ($response['id_report'] == $report['id_report']) {
                                $statusLabel = 'Completed';
                                break;
                            }
                        }
                        if ($statusLabel !== 'Completed') : ?>
                            <div class="col-md-4">
                                <h6>RESPONSE : </h6>
                                <form action="<?= BASEURL ?>/response/respondToReport" method="post" id="responseForm<?= $report['id_report'] ?>">
                                    <input type="text" name="id_report" id="" value="<?= $report['id_report'] ?>" hidden>
                                    <input type="date" name="date" id="" value="<?= date('Y-m-d') ?>" hidden>
                                    <input type="text" name="nis" id="" value="<?= $report['nis'] ?>" hidden>
                                    <textarea name="response" id="responseText<?= $report['id_report'] ?>" cols="35" rows="10" placeholder="Masukkan pesan Anda di sini..."></textarea>
                                    <div class="d-flex justify-content-end">
                                        <button type="submit" class="btn btn-warning text-light mt-2">Submit</button>
                                    </div>
                                </form>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endforeach ?>