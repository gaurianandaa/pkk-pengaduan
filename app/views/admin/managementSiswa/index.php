<!-- siswa -->

<div class="container m-3">
    <h1>Management Siswa</h1>

    <div class="row my-5">
        <div class="col-lg-6">
            <button class="btn bg-primary">
                <a href="<?= BASEURL ?>/managementSiswa/create" class="text-decoration-none text-light">
                    + Add Siswa
                </a>
            </button>
        </div>
        <div class="col-lg-6 d-flex justify-content-end">
            <form class="d-flex" action="<?= BASEURL ?>/managementSiswa/index" method="post">
                <input class="form-control me-2" type="search" placeholder="Search by NIS" aria-label="Search" style="width: fit-content;" name="search">
                <button class="btn btn-outline-primary" type="submit">Search</button>
            </form>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">NIS</th>
                <th scope="col">Name</th>
                <th scope="col">Phone</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <?php foreach ($data['user'] as $user) : ?>
            <?php if ($user['role'] === "Siswa") : ?>
                <tbody>
                    <tr>
                        <td scope="row"><?= $user['nis'] ?></td>
                        <td><?= $user['name'] ?></td>
                        <td><?= $user['phone'] ?></td>
                        <td>
                            <div class="row gap-2">
                                <!-- <button class="btn btn-primary p-2" style="width: fit-content;" data-toggle="modal" data-target="#edit<?= $user['nis'] ?>">
                                    Edit
                                </button> -->
                                <button class="btn btn-primary p-2" style="width: fit-content;" data-toggle="modal" data-target="#detail<?= $user['nis'] ?>">
                                    Edit
                                </button>
                                <a href="<?= BASEURL ?>/managementSiswa/prosesDelete/<?= $user['nis'] ?>" class="btn btn-danger p-2" style="width: fit-content;" onclick="return confirm('hapus?')">
                                    Delete
                                </a>
                            </div>
                        </td>
                    </tr>
                </tbody>
            <?php endif ?>
        <?php endforeach ?>
    </table>
</div>

<?php foreach ($data['user'] as $user) : ?>
    <div class="modal fade" id="detail<?= $user['nis'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Admin Update</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> ×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="<?= BASEURL; ?>/managementSiswa/update/" method="post">
                        <!-- <input type="hidden" name="nis" value="<?= $user['nis'] ?>"> -->
                        <div class="mb-3">
                            <label for="nis" class="form-label">Nis</label>
                            <input type="text" class="form-control" id="nis" value="<?= $user['nis'] ?>" name="nis">
                        </div>
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" class="form-control" id="name" value="<?= $user['name'] ?>" name="name">
                        </div>
                        <div class="mb-3">
                            <label for="phone" class="form-label">Phone</label>
                            <input type="tel" class="form-control" id="phone" value="<?= $user['phone'] ?>" name="phone">
                        </div>
                        <input type="text" class="form-control" id="role" value="<?= $user['role'] ?>" name="role" hidden>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

<?php endforeach ?>