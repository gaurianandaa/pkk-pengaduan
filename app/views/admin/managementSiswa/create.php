<!-- <div class="container">


    <div class="d-flex justify-content-center">
        <div class=col-lg-8>
            <div class="card border-0 shadow-sm">
                <div class="card-bodyp-0 ">
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block "></div>
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Create Siswa</h1>
                                </div>
                                <form action="<?= BASEURL; ?>/managemetAdmin/prosesCreate" method="post">
                                    <div class="form-group row mb-3">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <label for="" class="mb-2">Nis</label>
                                            <input type="text" class="form-control" placeholder="nis" name="nis" required>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="" class="mb-2">Name</label>
                                            <input type="text" class="form-control" placeholder="name" name="name" required>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-3">
                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                            <label for="" class="mb-2">Phone</label>
                                            <input type="text" class="form-control" placeholder="phone" name="phone" required>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="" class="mb-2">Password</label>
                                            <input type="password" class="form-control" placeholder="password" name="password" required>
                                        </div>
                                    </div>
                                    <div class="form-group mb-3">
                                        <input type="password" class="form-control" placeholder="password" name="role" value="Siswa" hidden>
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <button type="submit" class="btn btn-primary text-light mt-3">SAVE</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="container">
    <h1> Management Admin</h1>

    <div class="d-flex justify-content-center">
        <div class="col-xl-10 col-lg-12">
            <div class="card border-0 shadow-sm">
                <div class="card-bodyp-0">
                    <div class="row">
                        <div class="col-lg-12 p-5">
                            <div class="m-5">
                                <form action="<?= BASEURL; ?>/managementSiswa/prosesCreate" method="post">
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Nis</label>
                                        <input type="text" class="form-control" placeholder="nis" name="nis" required>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Name</label>
                                        <input type="text" class="form-control" placeholder="name" name="name" required>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Phone</label>
                                        <input type="text" class="form-control" placeholder="phone" name="phone" required>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Password</label>
                                        <input type="password" class="form-control" placeholder="password" name="password" required>
                                    </div>
                                    <div class="form-group mb-3">
                                        <input type="password" class="form-control" placeholder="password" name="role" value="Siswa" hidden>
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <button type="submit" class="btn bg-primary text-light">SAVE</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>