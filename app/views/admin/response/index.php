<!-- feedback -->

<div class="container m-3">
    <h1><?= $data['title'] ?></h1>

    <div class="row my-5">
        <div class="col-lg-12">
            <form action="" class="d-flex justify-content-end">
                <input class="form-control me-2" aria-label="Search" style="width: fit-content;" type="search" placeholder="Search" name="search">
                <button class="btn btn-outline-primary">search</button>
            </form>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th style="width: 10%;">No</th>
                <th style="width: 10%;">Report Id</th>
                <th style="width: 10%;">Nis</th>
                <th style="width: 40%;">Response</th>
                <th style="width: 15%;">Response Date</th>
                <th style="width: 15%;">Action</th>
            </tr>
        </thead>
        <?php foreach ($data['response'] as $response) : ?>
            <tbody>
                <tr>
                    <td><?= $response['id_feedback'] ?></td>
                    <td><?= $response['id_report'] ?></td>
                    <td><?= $response['nis'] ?></td>
                    <td><?= substr($response['response'], 0, 50); ?>...</td>
                    <td><?= $response['date'] ?></td>
                    <td>
                        <div class="row gap-2">
                            <button class="btn btn-primary p-2" style="width: fit-content;" data-toggle="modal" data-target="#detail<?= $response['id_feedback'] ?>">
                                View
                            </button>
                        </div>
                    </td>
                </tr>
            </tbody>
        <?php endforeach ?>
    </table>
</div>



<?php foreach ($data['response'] as $response) : ?>
    <div class="modal fade" id="detail<?= $response['id_feedback'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content container w-100">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Response</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> ×</span>
                    </button>
                </div>
                <?php foreach ($data['report'] as $report) : ?>
                    <?php if ($report['id_report'] == $response['id_report']) : ?>
                        <table class="table border-0 table-hover p-2">
                            <tbody>
                                <tr>
                                    <td style="width: 30%;">REPORT</td>
                                    <td><?= $report['isi_laporan'] ?></td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;">PROOF IMAGE</td>
                                    <td><img src="<?= HREF ?>/img/<?= $report['image'] ?>" alt="" style="width: 300px; height:300px;"></td>
                                </tr>

                            </tbody>
                        </table>
                    <?php endif; ?>
                <?php endforeach ?>
                <h5>Response</h5>
                <table class="table border-0 table-hover p-2">
                    <tbody>
                        <tr>
                            <td style="width: 30%;">Response Date</td>
                            <td><?= $response['date'] ?></td>
                        </tr>
                        <tr>
                            <td style="width: 30%;">Response</td>
                            <td><?= $response['response'] ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php endforeach ?>