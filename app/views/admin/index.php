<!-- dashboard -->

<div class="container m-3">
    <h1>Dashboard</h1>
    <div class="row d-flex justify-content-center my-5 ">
        <div class="col-lg-4">
            <div class="card border-0 shadow-lg">
                <div class="card-body">
                    <h4>ADMINS</h4>
                    <h6><?= $data['countAdmin'] ?></h6>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card border-0 shadow-lg">
                <div class="card-body">
                    <h4>STUDENTS</h4>
                    <h6><?= $data['count'] ?></h6>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card border-0 shadow-lg">
                <div class="card-body">
                    <h4>REPORTS</h4>
                    <h6><?= $data['countReport'] ?></h6>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="row d-flex justify-content-center">
            <div class="card border-0 shadow-lg">
                <div class="card-body">
                    <h4>Admin</h4>

                    <div class="my-4">
                        <?php foreach ($data['detail'] as $detail) : ?>
                            <?php if ($detail['role'] === "Admin") : ?>
                            <div class="row mx-5 mb-3">
                                <div class="col-lg-1">
                                    <img src="<?= HREF ?>/img/admin/profileDummy.png" alt="" style="width: 80px;">
                                </div>
                                <div class="col-lg-6 d-flex align-items-center justify-content-start ms-3">
                                    <div class="name">
                                        <h6 class="text-dark"><?= $detail['name'] ?></h6>
                                        <h6 class="fw-dark fst-italic text-secondary"><?= $detail['role'] ?></h6>
                                    </div>
                                </div>
                                <div class="col-lg-4 d-flex align-items-center justify-content-end">
                                    <button class="btn btn-primary p-2" style="width: fit-content;" data-toggle="modal" data-target="#detail<?= $ppk['nis'] ?>">
                                        Detail
                                    </button>
                                </div>
                            </div>
                            <?php endif ?>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php foreach ($data['detail'] as $admin) : ?>
    <div class="modal fade" id="detail<?= $admin['nis'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Admin Detail</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> ×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="nis" value="<?= $admin['nis'] ?>">
                    <div class="mb-3">
                        <label for="name" class="form-label">Name</label>
                        <input type="text" class="form-control" id="name" value="<?= $admin['name'] ?>" name="name">
                    </div>
                    <div class="mb-3">
                        <label for="phone" class="form-label">Phone</label>
                        <input type="tel" class="form-control" id="phone" value="<?= $admin['phone'] ?>" name="phone">
                    </div>
                    <div class="mb-3">
                        <label for="role" class="form-label">Role</label>
                        <input type="text" class="form-control" id="role" value="<?= $admin['role'] ?>" name="role">
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>