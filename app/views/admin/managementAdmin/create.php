<div class="container">
    <h1> <?= $data['title']?></h1>

    <div class="d-flex justify-content-center">
        <div class="col-xl-10 col-lg-12">
            <div class="card border-0 shadow-sm">
                <div class="card-bodyp-0">
                    <div class="row">
                        <div class="col-lg-12 p-5">
                            <div class="m-5">
                                <form action="<?= BASEURL; ?>/managementAdmin/prosesCreate" method="post">
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Nip</label>
                                        <input type="text" class="form-control" placeholder="nip" name="nis" required>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Name</label>
                                        <input type="text" class="form-control" placeholder="name" name="name" required>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Phone</label>
                                        <input type="text" class="form-control" placeholder="phone" name="phone" required>
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Password</label>
                                        <input type="password" class="form-control" placeholder="password" name="password" required>
                                    </div>
                                    <div class="form-group mb-3">
                                        <input type="password" class="form-control" placeholder="password" name="role" value="Admin" hidden>
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <button type="submit" class="btn bg-primary text-light">SAVE</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>