 <?php
    class User_model
    {
        private $table = 'users';
        private $db;

        public function __construct()
        {
            $this->db = new Database;
        }

        public function register($data)
        {
            $query = "INSERT INTO users (nis, name, phone, role, password) VALUES (:nis, :name, :phone, :role, :password)";
            $passwordHash = password_hash($data['password'], PASSWORD_DEFAULT);
            $this->db->query($query);
            $this->db->bind('nis', $data['nis']);
            $this->db->bind('name', $data['name']);
            $this->db->bind('phone', $data['phone']);
            $this->db->bind('role', $data['role']);
            $this->db->bind('password', $passwordHash);
            $this->db->execute();

            return 1;
        }
        public function update($data)
        {
            $nis = $data['nis'];
            $name = $data['name'];
            $phone = $data['phone'];
            $role = $data['role'];

            $query = "UPDATE users SET name = :name, phone = :phone, role = :role WHERE nis = :nis";
            $this->db->query($query);
            $this->db->bind("nis", $nis);
            $this->db->bind("name", $name);
            $this->db->bind("phone", $phone);
            $this->db->bind("role", $role);
            $this->db->execute();

            return "success";
        }

        public function delete($nis)
        {
            $query = "DELETE FROM users WHERE nis = :nis";
            $this->db->query($query);
            $this->db->bind('nis', $nis);
            $this->db->execute();

            return $this->db->rowCount();;
        }
        public function getCountUser()
        {
            $query = "SELECT COUNT(*) as count FROM users WHERE role = 'Siswa'";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            return $result['count'];
        }
        public function getCountAdmin()
        {
            $query = "SELECT COUNT(*) as count FROM users WHERE role = 'Admin'";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            return $result['count'];
        }

        public function getUserAdmin()
        {
            if (isset($_POST['search']) && !empty($_POST['search'])) {
                $nis = $_POST['search'];
                $this->db->query('SELECT * FROM ' . $this->table . ' WHERE nis = :nis');
                $this->db->bind(':nis', $nis);
            } else {
                $this->db->query('SELECT * FROM ' . $this->table);
            }
            $results = $this->db->resultSet();
    
            if (empty($results)) {
                $referer = $_SERVER['HTTP_REFERER'];
                echo '<script>';
                echo 'alert("Not Found.");';
                echo 'window.location.href = "' . BASEURL . '/managementAdmin";';
                echo '</script>';
            }
            
            return $results;
        }
        public function getUserSiswa()
        {
            if (isset($_POST['search']) && !empty($_POST['search'])) {
                $nis = $_POST['search'];
                $this->db->query('SELECT * FROM ' . $this->table . ' WHERE nis = :nis');
                $this->db->bind(':nis', $nis);
            } else {
                $this->db->query('SELECT * FROM ' . $this->table);
            }
            $results = $this->db->resultSet();
    
            if (empty($results)) {
                $referer = $_SERVER['HTTP_REFERER'];
                echo '<script>';
                echo 'alert("Not Found.");';
                echo 'window.location.href = "' . BASEURL . '/managementSiswa";';
                echo '</script>';
            }
            
            return $results;
        }
        public function login($data)
        {
            $nis = $data['nis'];
            $query = "SELECT * FROM users WHERE nis = :nis";
            $this->db->query($query);
            $this->db->bind(':nis', $nis);
            $user = $this->db->single();
            $count = $this->db->rowCount();

            if ($count === 0) {
                return "kosong";
            }

            if ($user) {
                $passwordPost = $data['password'];
                $passwordDb = $user['password'];

                if (password_verify($passwordPost, $passwordDb)) {
                    return $user;
                }
            }

            return false;
        }
        public function showUser()
        {
            
        }
    }
