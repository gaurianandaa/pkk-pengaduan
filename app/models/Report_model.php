<?php

class Report_model

{
    private $table = 'reports';
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }
    public function reportCreate($data)
    {
        if (isset($data['date']) && isset($data['isi_laporan']) && isset($_FILES['image']['name']) && isset($data['nis']) && isset($data['status'])) {
            $date = $data['date'];
            $isi_laporan = $data['isi_laporan'];
            $image_name = $_FILES['image']['name'];
            $image_tmp = $_FILES['image']['tmp_name'];
            $nis = $data['nis'];
            $status = $data['status'];

            $allowedExt = array("jpg", "png", "jpeg", "svg");
            $extGambar = pathinfo($image_name, PATHINFO_EXTENSION);

            if (in_array($extGambar, $allowedExt)) {
                $uniqueFileName = time() . '-' . $image_name;

                $upload_path = '../public/img/' . $uniqueFileName;

                if (move_uploaded_file($image_tmp, $upload_path)) {
                    $query = "INSERT INTO reports (date, isi_laporan, image, nis, status) VALUES(:date, :isi_laporan, :image, :nis, :status)";
                    $this->db->query($query);
                    $this->db->bind('date', $date);
                    $this->db->bind('isi_laporan', $isi_laporan);
                    $this->db->bind('image', $uniqueFileName);
                    $this->db->bind('nis', $nis);
                    $this->db->bind('status', $status);

                    $this->db->execute();
                    return 1;
                } else {
                    return -1; // Error in file upload
                }
            } else {
                return -2; // Invalid file extension
            }
        } else {
            return -3; // Missing data keys
        }
    }
    public function getCountReport()
    {
        $query = "SELECT COUNT(*) as count FROM reports";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result['count'];
    }
    public function getReport()
    {
        if (isset($_POST['search']) && !empty($_POST['search'])) {
            $nis = $_POST['search'];
            $this->db->query('SELECT * FROM ' . $this->table . ' WHERE nis = :nis');
            $this->db->bind(':nis', $nis);
        } else {
            $this->db->query('SELECT * FROM ' . $this->table);
        }
        return $this->db->resultSet();

    }
    public function getResponse()
    {
        if (isset($_POST['search']) && !empty($_POST['search'])) {
            $id_response = $_POST['search'];
            $this->db->query('SELECT * FROM responses, reports WHERE id_response = :id_response');
            $this->db->bind(':id_response', $id_response);
        } else {
            $this->db->query('SELECT * FROM responses');
        }
        
        return $this->db->resultSet();

    }
    public function saveResponse($data) {
        // Query untuk menyimpan respon ke dalam database
        $query = "INSERT INTO responses (id_report, response, date, nis) VALUES(:id_report, :response, :date, :nis)";
        $this->db->query($query);
        $this->db->bind('id_report', $data['id_report']);
        $this->db->bind('response', $data['response']);
        $this->db->bind('date', $data['date']);
        $this->db->bind('nis', $data['nis']);
        $this->db->execute();

        return 1;
    }
}
