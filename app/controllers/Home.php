<?php 

class Home extends Controller
{

    public function __construct()
    {
        if (!$_SESSION['login']) {
            header('location:' . BASEURL . '/login');
        }
    }

    public function index(){
        $data['title'] = 'Home';
        $this->view('home/index', $data);
    }
    public function login(){
        $this->view('login/index');
    }
}