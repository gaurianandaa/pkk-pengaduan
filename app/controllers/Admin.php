<?php

class Admin extends Controller
{
    public function __construct()
    {
        if (!$_SESSION['login']) {
            header('location:' . BASEURL . '/login');
        }
        
        if ($_SESSION['role'] !== 'admin') {
            header('Location: ' . BASEURL . '/user');
            exit;
        }

    }
    public function index()
    {
        $data['count'] = $this->model('User_model')->getCountUser();
        $data['countAdmin'] = $this->model('User_model')->getCountAdmin();
        $data['countReport'] = $this->model('Report_model')->getCountReport();
        $data['detail'] = $this->model('User_model')->getUserAdmin();
        $data['title'] = 'Dashboard';
        $this->view('templates/sidebar', $data);
        $this->view('admin/index', $data);
        $this->view('templates/endsidebar');
    }
    public function detail()
    {
        $data['detail'] = $this->model('User_model')->getUser();
        $this->view('templates/sidebar');
        $this->view('admin/index', $data);
        $this->view('templates/endsidebar');
    }
}
