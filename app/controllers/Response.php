<?php

class Response extends Controller
{
    public function index()
    {
        $data['title'] = 'Respond';
        $data['report'] = $this->model('Report_model')->getReport();
        $data['response'] = $this->model('Report_model')->getResponse();
        $this->view('templates/sidebar', $data,);
        $this->view('admin/response/index', $data);
        $this->view('templates/endsidebar');
    }
    public function respondToReport() {
        // var_dump($_POST);
        // die;
        if ($this->model('Report_model')->saveResponse($_POST) > 0) {
            header('Location: ' . BASEURL . '/response');
        }
    }
}

