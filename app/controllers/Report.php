<?php 

class Report extends Controller{
    public function index(){
        $data['title'] = 'Report';
        $data['report'] = $this->model('Report_model')->getReport();
        $data['response'] = $this->model('Report_model')->getResponse();
        $this->view('templates/sidebar',$data);
        $this->view('admin/report/index', $data);
        $this->view('templates/endsidebar');
    }

}