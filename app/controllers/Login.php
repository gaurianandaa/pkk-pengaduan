<?php

class Login extends Controller
{
    public function auth()
    {
        $formData = $_POST;
        $userModel = $this->model('User_model');

        $user = $userModel->login($formData);
        // var_dump($user);
        // die;

        if ($user === "kosong") {
            echo '<script>';
            echo 'alert("NIS tidak ditemukan.");';
            echo 'window.location.href = "' . BASEURL . '/login";';
            echo '</script>';
        } else if ($user != null) {
            $_SESSION['login'] = true;
            $_SESSION['role'] = strtolower($user['role']); // Simpan peran pengguna dalam sesi

            if ($_SESSION['role'] === 'admin') {
                header('Location: ' . BASEURL . '/admin');
            } else {
                header('Location: ' . BASEURL . '/user');
            }
        } else {
            // Login gagal
            echo '<script>';
            echo 'alert("Login gagal. Silakan cek kembali informasi login Anda.");';
            echo 'window.location.href = "' . BASEURL . '/login";';
            echo '</script>';
        }
    }

    public function index()
    {
        $data['title'] = 'Login';
        $this->view('login/index', $data);
    }

    public function logout()
    {
        session_destroy();
        header('Location: ' . BASEURL . '/login');
        exit;
    }
}
