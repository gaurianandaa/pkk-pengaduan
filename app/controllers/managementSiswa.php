<?php 

class managementSiswa extends Controller{
    public function index(){
        $data['user'] = $this->model('User_model')->getUserSiswa();
        $data['title'] = 'Management Siswa';
        $this->view('templates/sidebar', $data);
        $this->view('admin/managementSiswa/index', $data);
        $this->view('templates/endsidebar');
    }
    public function create(){
        $data['title'] = 'Siswa';
        $this->view('templates/sidebar', $data);
        $this->view('admin/managementSiswa/create');
        $this->view('templates/endsidebar');
    }
    public function update()
    {
        $result = $this->model("User_model")->update($_POST);

        if ($result === "success") {
            echo "
            <script>
                alert('Berhasil update cuyy😊😊☝️');
                window.location.href = 'http://localhost/pkk-pengaduan/managementSiswa/';
            </script>
            ";
        }
    }
    public function prosesCreate()
    {
        // var_dump($_POST);
        // die;
        if ($this->model('User_model')->register($_POST) > 0) {
            header('Location: ' . BASEURL . '/managementSiswa');
        }
    }
    public function prosesDelete($nis)
    {
        if($this->model('User_model')->delete($nis) > 0){
            echo '<script>';
            echo 'alert("Delete sukses");';
            echo 'window.location.href = "' . BASEURL . '/managementSiswa";';
            echo '</script>';
        } else{
            echo '<script>';
            echo 'alert("Delete gagal.");';
            echo 'window.location.href = "' . BASEURL . '/admin";';
            echo '</script>';
        }
    }
}