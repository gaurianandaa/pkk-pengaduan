<?php

class managementAdmin extends Controller
{
    public function index()
    {
        $data['user'] = $this->model('User_model')->getUserAdmin();
        $data['title'] = 'Management Admin';
        $this->view('templates/sidebar', $data);
        $this->view('admin/managementAdmin/index', $data);
        $this->view('templates/endsidebar');
    }
    public function create()
    {
        $data['title'] = 'Create Admin';
        $this->view('templates/sidebar', $data);
        $this->view('admin/managementAdmin/create', $data);
        $this->view('templates/endsidebar');
    }
    public function prosesCreate()
    {
        // var_dump($_POST);
        // die;
        if ($this->model('User_model')->register($_POST) > 0) {
            header('Location: ' . BASEURL . '/managementAdmin');
        }
    }
    public function update()
    {
        $result = $this->model("User_model")->update($_POST);

        if ($result === "success") {
            echo "
            <script>
                alert('Update Berhasil');
                window.location.href = 'http://localhost/pkk-pengaduan/managementAdmin/';
            </script>
            ";
        }
    }

    public function prosesDelete($nis)
    {
        if($this->model('User_model')->delete($nis) > 0){
            echo '<script>';
            echo 'alert("Delete sukses");';
            echo 'window.location.href = "' . BASEURL . '/managementAdmin";';
            echo '</script>';
        } else{
            echo '<script>';
            echo 'alert("Delete gagal.");';
            echo 'window.location.href = "' . BASEURL . '/admin";';
            echo '</script>';
        }
    }
}
