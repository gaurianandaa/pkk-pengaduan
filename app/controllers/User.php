
<?php

class User extends Controller
{

    public function __construct()
    {
        if (!$_SESSION['login']) {
            header('location:' . BASEURL . '/login');
        }
        
        if ($_SESSION['role'] !== 'siswa') {
            header('Location: ' . BASEURL . '/admin');
            exit;
        }
    }
    public function index()
    {
        // $data['data'] = $this->model('User_model')->getUser();
        $data['title'] = 'Dashboard';
        $this->view('templates/sideuser', $data);
        $this->view('user/index');
        $this->view('templates/enduser');
    }
    public function create()
    {
        $data['title'] = 'Create Report';
        $this->view('templates/sideuser', $data);
        $this->view('user/create');
        $this->view('templates/enduser');
    }
    // controller pake disini aja biar ga buang buang controller kalo cuma isi index doang
    public function history()
    {
        $data['response'] = $this->model('Report_model')->getResponse();
        $data['history'] = $this->model('Report_model')->getReport();
        $data['title'] = 'Report History';
        $this->view('templates/sideuser', $data);
        $this->view('user/history', $data);
        $this->view('templates/enduser');
    }
    public function prosesCreate()
    {
        // var_dump($_POST);
        // die;
        if( $this->model('Report_model')->createReport($_POST) > 0){
            header('Location:' . BASEURL . '/user');
            exit;
        }
    }
    public function createReport()
    {
        if ($this->model('Report_model')->reportCreate($_POST) > 0) {
            // header('Location:' . BASEURL . '/user');
            echo '<script>';
            echo 'alert("Laporan anda terkirim!");';
            echo 'window.location.href = "' . BASEURL . '/user"';
            echo '</script>';
        } else {
            // header('Location: ' . BASEURL . '/user/create');
            echo '<script>';
            echo 'alert("Laporan anda gagal terikirim!");';
            echo 'window.location.href = "' . BASEURL . '/user/create"';
            echo '</script>';
        }
    }
}
